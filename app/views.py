# views.py

from flask import make_response, render_template, request, Blueprint, jsonify
from app import app
from random import shuffle
from PIL import Image
import numpy as np
import os

print(app.static_folder)
uploadDir = './Uploads/'

@app.route('/')
def home():
    resp=make_response(render_template("index.html"))
    resp.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    resp.headers["Pragma"] = "no-cache"
    resp.headers["Expires"] = "0"
    return resp

@app.route('/board', methods = ['POST'])
def success():  
    if request.method == 'POST':
        files = request.files.getlist("images")
        filenames = list() 
        for file in files:
            file.save(app.static_folder+'/img/'+file.filename)
            filenames.append(file.filename)
            filenames.append(file.filename)
        shuffle(filenames) 
        resp=make_response(render_template("board.html", filenames=filenames))
        return resp
    return render_template("index.html") 

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('index.html'), 404


@app.route('/compare', methods = ['POST'])
def compare():
    if request.method == 'POST':
        req = request.get_json()
        status = comparador(req[0], req[1])
        print(status)
        res = make_response(jsonify({"message": str(status)}), 200)
        return res
    print("Método no aceptado")

def comparador(image1, image2):
    print(image1+" "+image2)
    img = Image.open(image1)

    img1r = np.asarray(img.convert("RGB", (1,0,0,0, 1,0,0,0, 1,0,0,0) ))
    img1g = np.asarray(img.convert("RGB", (0,1,0,0, 0,1,0,0, 0,1,0,0) ))
    img1b = np.asarray(img.convert("RGB", (0,0,1,0, 0,0,1,0, 0,0,1,0) ))

    hr, h_bins = np.histogram(img1r, bins=256, density=True)
    hg, h_bins = np.histogram(img1g, bins=256, density=True)
    hb, h_bins = np.histogram(img1b, bins=256, density=True)

    hist1 = np.array([hr, hg, hb]).ravel()

    img = Image.open(image2)

    img2r = np.asarray(img.convert("RGB", (1,0,0,0, 1,0,0,0, 1,0,0,0) ))
    img2g = np.asarray(img.convert("RGB", (0,1,0,0, 0,1,0,0, 0,1,0,0) ))
    img2b = np.asarray(img.convert("RGB", (0,0,1,0, 0,0,1,0, 0,0,1,0) ))

    hr, h_bins = np.histogram(img2r, bins=256, density=True)
    hg, h_bins = np.histogram(img2g, bins=256, density=True)
    hb, h_bins = np.histogram(img2b, bins=256, density=True)

    hist2 = np.array([hr, hg, hb]).ravel()

    diff = hist1 - hist2
    distance =  np.sqrt(np.dot(diff, diff))
    print(distance)

    if(distance == 0):
        return True
    else:
        return False
